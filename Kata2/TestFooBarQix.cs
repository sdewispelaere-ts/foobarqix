﻿namespace Kata2
{
    using NUnit.Framework;

    public class TestFooBarQix
    {
        private FooBarQix _fooBarQix;

        [SetUp]
        public void Setup()
        {
            _fooBarQix = new FooBarQix();
        }

        [TestCase(1, "1")]
        [TestCase(2, "2")]
        [TestCase(4, "4")]
        [TestCase(8, "8")]
        public void FooBarQix_NumberObeyNoRule_DisplayIt(int value, string expected)
        {
            Assert.AreEqual(expected, this._fooBarQix.Get(value));
        }

        [TestCase(3)]
        [TestCase(6)]
        [TestCase(9)]
        public void FooBarQix_NumberIsDivisibleByThree_DisplayFoo(int value)
        {
            Assert.IsTrue(this._fooBarQix.Get(value).Contains("Foo"));
        }

        [TestCase(3)]
        [TestCase(13)]
        [TestCase(23)]
        [TestCase(33)]
        public void FooBarQix_NumberContainsThree_DisplayFoo(int value)
        {
            Assert.IsTrue(this._fooBarQix.Get(value).Contains("Foo"));
        }

        [TestCase(3, "FooFoo")]
        public void FooBarQix_NumberContainsThreeAndIsDivisibleByThree_DisplayFooFoo(int value, string expected)
        {
            Assert.AreEqual(expected, this._fooBarQix.Get(value));
        }

        [TestCase(5)]
        [TestCase(10)]
        [TestCase(15)]
        public void FooBarQix_NumberIsDivisibleByFive_DisplayBar(int value)
        {
            Assert.IsTrue(this._fooBarQix.Get(value).Contains("Bar"));
        }

        [TestCase(5)]
        [TestCase(15)]
        [TestCase(25)]
        [TestCase(35)]
        public void FooBarQix_NumberContainsFive_DisplayBar(int value)
        {
            Assert.IsTrue(this._fooBarQix.Get(value).Contains("Bar"));
        }

        [TestCase(7)]
        [TestCase(14)]
        [TestCase(21)]
        public void FooBarQix_NumberIsDivisibleBySeven_DisplayQix(int value)
        {
            Assert.IsTrue(this._fooBarQix.Get(value).Contains("Qix"));
        }

        [TestCase(7)]
        [TestCase(17)]
        [TestCase(27)]
        [TestCase(37)]
        public void FooBarQix_NumberContainsSeven_DisplayQix(int value)
        {
            Assert.IsTrue(this._fooBarQix.Get(value).Contains("Qix"));
        }

        [Test]
        public void FooBarQix_DividersBeforeContent()
        {
            Assert.AreEqual("FooBar", this._fooBarQix.Get(51));
        }

        [Test]
        public void FooBarQix_DigitRuleIsTreatedIsTheRightOrder()
        {
            Assert.AreEqual("BarFoo", this._fooBarQix.Get(53));
        }

        [Test]
        public void FooBarQix_DivideRuleIsTreatedIsTheRightOrder()
        {
            Assert.AreEqual("FooQix", this._fooBarQix.Get(21));
        }

        [Test]
        public void FooBarQix_MultipleDividersAndDigits()
        {
            Assert.AreEqual("FooBarBar", this._fooBarQix.Get(15));
        }

        [Test]
        public void FooBarQix_MultipleDividersAndDigitsAll_Foo()
        {
            Assert.AreEqual("FooFooFoo", this._fooBarQix.Get(33));
        }

        [TestCase(105, "FooBarQixBar")] // Divisable by 3, 5, and 7, have a 5
        [TestCase(315, "FooBarQixFooBar")] // Divisable by 3, 5, and 7, have a 3 and a 5
        [TestCase(735, "FooBarQixQixFooBar")] // Divisable by 3, 5, and 7, have a 3, 5, and 7
        public void FooBarQix_MiscTest(int value, string expected)
        {
            Assert.AreEqual(expected, this._fooBarQix.Get(value));   
        }
    }
}
